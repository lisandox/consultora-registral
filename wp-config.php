<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'Consultora-Registral' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

if ( !defined('WP_CLI') ) {
    define( 'WP_SITEURL', $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] );
    define( 'WP_HOME',    $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] );
}



/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'TH42u8KXmnz91DkzVYlCT2g7YjSJqCWTyQkzl0BbudjvRYQTPnDFVDYhxjNtV9aN' );
define( 'SECURE_AUTH_KEY',  'DDz6Te1RtRb03v8QI0npJIVpotMSuTCHD258bEjWE081IsDdvArWr4BNSwYRyVM6' );
define( 'LOGGED_IN_KEY',    '1CZivz0fXs51PK6D7rLKPVxsP1j59KiPMmnthRIfLNGDzKV6WCsw39Z5hyJLW3KQ' );
define( 'NONCE_KEY',        'zAtWwNlTK1ksfitacbtjOnFDfmwJuPZioBTWLIhhTAmHVCtHZKnX7nut0bbqru4m' );
define( 'AUTH_SALT',        '8yWAx2yxxxzTFQQGhUcSMPOOGHJpQTFL1RpoezQj3n3Lhv2i4Rin0D1S2OE5WWlT' );
define( 'SECURE_AUTH_SALT', 'xVD8jIwSMNqpSjw5xDQqIRQ7CJ3HrrmOLdqMnNCBeONfT3e9qVIDPF7Qv8Q4YU5n' );
define( 'LOGGED_IN_SALT',   'a7I0SWnFoF38tjQOWcmyF2ZnIIM4doKWqYSMhAJKsKbJpVzOmVGcTf3iJrmzPWug' );
define( 'NONCE_SALT',       '9oyWQhwMAJXmyIJqjeYLeNC5Ed9MYAs3C6zsyHZdHCjvwdxNBgM1YOwEDwHMdVa4' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
